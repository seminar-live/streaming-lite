# General

* Via Service `usbip.service` wird der usbipd gestartet.
* Ein Notebook loggt sich via root ein, usb port unbind, usbipd restart, usb port bind.

# Setup

* usbip-reshare.sh >> /usr/local/bin/usbip-reshare.sh  
* usbip.service >> /etc/systemd/system/usbip.service

``` shell
# systemd neu laden:
$ systemctl daemon-reload

# Testweise Service starten:
$ systemctl start usbip.service

# Service beim Boot automatisch starten
$ systemctl enable usbip.service
```
