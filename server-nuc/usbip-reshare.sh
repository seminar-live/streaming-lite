#!/bin/bash
#
# CR / 19.9.20
#
# usbip-reshare.sh [usb-identifier1] [usb-identifier2] ... [usb-identifierN]
# Bsp: usbip-reshare.sh 28e6:0024 534d:2109
#
# Devio: 28e6:0024
# RULLZ: 534d:2109

# Unbind
for II in $@ ; do
  # 'busid=3-1.2#usbid=534d:2109#' >> 3-1.2
  USB_PORT=`usbip list -pl | grep $II | egrep -o '[0-9]+-[0-9]+\.?[0-9]*'`
  [ $? -ne 0 -o -z "$USB_PORT" ] && echo "Not found USB-identifier: $II" && exit 1
  usbip unbind -b $USB_PORT 2> /dev/null
done

# USBIP neustarten (mitunter funktioniert sonst das bind nicht)
systemctl restart usbip.service

# Bind
for II in $@ ; do
  USB_PORT=`usbip list -pl | grep $II | egrep -o '[0-9]+-[0-9]+\.?[0-9]*'`
  [ $? -ne 0 -o -z "$USB_PORT" ] && echo "Not found USB-identifier: $II" && exit 1
  usbip bind -b $USB_PORT 2> /dev/null

  # Echo USB_PORT to client to attach.
  echo $USB_PORT
done

for II in $@ ; do
  logger -p syslog.notice "usbip-reshare $II" Sender: $SSH_CLIENT
done
