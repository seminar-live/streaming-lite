#!/bin/bash

USB_SERVER=sln`cat /etc/seminarlive/room`.math.uzh.ch

# Capture Card RULLZ, DEVIO
USB_PORTS=`ssh -oStrictHostKeyChecking=accept-new root@$USB_SERVER /usr/local/bin/usbip-reshare.sh 534d:2109 28e6:0024`

for II in $USB_PORTS ; do 
  usbip attach -r $USB_SERVER -b $II
done

sleep 1

# Ports
/usr/bin/usbip port
