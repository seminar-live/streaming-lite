# General

* Beim Boot wird via Service `usbip-attach.service` gestartet. 
* Der Service startet `/usr/local/bin/usbip-attach.sh`:

  * SSH root Login auf dem Raum NUC, mit Angabe welche USB Device IDs freigegeben/attached werden sollen.

    * Bestehende USB Port Freigaben loeschen
    * `usbipd` neu starten (sonst werden die Ports nicht sicher freigegeben)
    * USB Port freigeben.
  
  * USB Port attach.

# Setup

https://git.math.uzh.ch/seminar-live/streaming-lite/-/tree/master/client-notebook:

 * set-devio-default.sh > /usr/local/bin/set-devio-default.sh
 * usbip-attach.sh > /usr/local/bin/usbip-attach.sh
 * usbip-attach-resume.service > /etc/systemd/system/usbip-attach-resume.service
 * usbip-attach.service > /etc/systemd/system/ usbip-attach.service

```shell
# Raum konfigurieren:
$ mkdir -p /etc/seminarlive; echo 'y27h12' > /etc/seminarlive/room

# systemd neu laden:
$ systemctl daemon-reload

# Testweise Service starten:
$ systemctl start usbip-attach.service

# Service beim Boot automatisch starten
$ systemctl enable usbip-attach.service
$ systemctl enable usbip-attach-resume.service

# User 'math' and 'matha' configure Startup Skript:
/usr/local/bin/set-devio-default.sh
```

