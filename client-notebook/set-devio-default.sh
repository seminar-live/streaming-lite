#!/bin/bash

#
# Wait until the Devio is available
i=0
while [ $i -le 10 ]; do
  ((i=i+1))
  /usr/bin/pactl list short sinks | grep Devio > /dev/null
  [ $? -eq 0 ] && break
  sleep 1
done

# Set default SPEAKER
DEVIO_SINK=`/usr/bin/pactl list short sinks| grep Devio | awk '{ print $2}'`
pactl set-default-sink $DEVIO_SINK

# Set default MICROPHONE
DEVIO_SOURCE=`/usr/bin/pactl list short sources| grep 'input\.usb-Biamp' | awk '{ print $2}'`
pactl set-default-source $DEVIO_SOURCE

echo "done"
